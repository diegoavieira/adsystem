# Angular Design System

[![pipeline status](https://gitlab.com/diegoavieira/adsystem/badges/master/pipeline.svg)](https://gitlab.com/diegoavieira/adsystem/commits/master)
[![coverage report](https://gitlab.com/diegoavieira/adsystem/badges/master/coverage.svg)](https://gitlab.com/diegoavieira/adsystem/commits/master)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)
[![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lernajs.io/)

[Demo](https://diegoavieira.gitlab.io/adsystem)
