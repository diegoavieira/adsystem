# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.0.0](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.33...v1.0.0) (2020-10-28)

**Note:** Version bump only for package @adsystem/shared

## [0.0.33](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.32...v0.0.33) (2020-10-26)

**Note:** Version bump only for package @adsystem/shared

## [0.0.32](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.31...v0.0.32) (2020-10-26)

**Note:** Version bump only for package @adsystem/shared

## [0.0.31](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.30...v0.0.31) (2020-10-26)

**Note:** Version bump only for package @adsystem/shared

## [0.0.30](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.29...v0.0.30) (2020-10-26)

**Note:** Version bump only for package @adsystem/shared

## [0.0.29](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.28...v0.0.29) (2020-10-26)

**Note:** Version bump only for package @adsystem/shared

## [0.0.28](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.27...v0.0.28) (2020-10-26)

**Note:** Version bump only for package @adsystem/shared

## [0.0.27](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.26...v0.0.27) (2020-10-26)

**Note:** Version bump only for package @adsystem/shared

## [0.0.26](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.25...v0.0.26) (2020-10-26)

**Note:** Version bump only for package @adsystem/shared

## [0.0.25](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.24...v0.0.25) (2020-10-26)

**Note:** Version bump only for package @adsystem/shared

## [0.0.24](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.23...v0.0.24) (2020-10-26)

**Note:** Version bump only for package @adsystem/shared

## [0.0.23](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.22...v0.0.23) (2020-10-26)

**Note:** Version bump only for package @adsystem/shared

## [0.0.22](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.21...v0.0.22) (2020-10-26)

**Note:** Version bump only for package @adsystem/shared

## [0.0.21](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.20...v0.0.21) (2020-10-23)

**Note:** Version bump only for package @adsystem/shared

## [0.0.20](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.19...v0.0.20) (2020-10-23)

**Note:** Version bump only for package @adsystem/shared

## [0.0.19](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.18...v0.0.19) (2020-10-23)

**Note:** Version bump only for package @adsystem/shared

## [0.0.18](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.17...v0.0.18) (2020-10-23)

**Note:** Version bump only for package @adsystem/shared

## [0.0.17](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.16...v0.0.17) (2020-10-23)

**Note:** Version bump only for package @adsystem/shared

## [0.0.16](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.15...v0.0.16) (2020-10-23)

**Note:** Version bump only for package @adsystem/shared

## [0.0.15](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.14...v0.0.15) (2020-10-23)

**Note:** Version bump only for package @adsystem/shared

## [0.0.14](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.12...v0.0.14) (2020-10-23)

### Bug Fixes

- adjustments ([0a90245](https://gitlab.com/diegoavieira/adsystem/commit/0a90245e3ed6734ad0c2d0ae1eed94fca82a6164))

## [0.0.13](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.12...v0.0.13) (2020-10-23)

**Note:** Version bump only for package @adsystem/shared

## [0.0.12](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.11...v0.0.12) (2020-10-23)

**Note:** Version bump only for package @adsystem/shared

## [0.0.11](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.10...v0.0.11) (2020-10-23)

**Note:** Version bump only for package @adsystem/shared

## [0.0.10](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.9...v0.0.10) (2020-10-23)

**Note:** Version bump only for package @adsystem/shared

## [0.0.9](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.8...v0.0.9) (2020-10-22)

**Note:** Version bump only for package @adsystem/shared

## [0.0.8](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.7...v0.0.8) (2020-07-07)

**Note:** Version bump only for package @adsystem/shared

## [0.0.7](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.6...v0.0.7) (2020-06-04)

### Bug Fixes

- adjustments ([7f4e229](https://gitlab.com/diegoavieira/adsystem/commit/7f4e2291640ee1ed155f2c22bf3c8da6202d8af3))

## [0.0.6](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.5...v0.0.6) (2020-04-30)

### Bug Fixes

- created doc-viewer ([420e3fa](https://gitlab.com/diegoavieira/adsystem/commit/420e3fa4c03335e39b5033cd594561af037e4848))
- created shared lib ([6d9450a](https://gitlab.com/diegoavieira/adsystem/commit/6d9450a96db772532943e88e78a4f3947a6a722c))
