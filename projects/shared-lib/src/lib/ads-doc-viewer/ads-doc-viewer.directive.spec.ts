import { AdsDocViewerDirective } from './ads-doc-viewer.directive';
import { Component, ElementRef, Renderer2, Type, Directive } from '@angular/core';
import { TestBed, ComponentFixture, fakeAsync } from '@angular/core/testing';
import { AdsDocViewerService } from './ads-doc-viewer.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

const expectedMdFile = '# Test';
const expectedHtmlFile = '<h1>Test</h1>\n';

@Component({})
class TestComponent {
  mdFile = expectedMdFile;
}

describe('AdsDocViewerDirective', () => {
  let component: TestComponent;
  let element: ElementRef;
  let renderer: Renderer2;
  let adsDocViewerService: AdsDocViewerService;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(() => {
    TestBed.overrideComponent(TestComponent, {
      set: {
        template: `<div [adsDocViewer]="mdFile"></div>`
      }
    });

    TestBed.configureTestingModule({
      declarations: [TestComponent, AdsDocViewerDirective],
      imports: [HttpClientTestingModule],
      providers: [AdsDocViewerService, HttpClientTestingModule]
    });

    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
    renderer = fixture.componentRef.injector.get<Renderer2>(Renderer2 as Type<Renderer2>);
    adsDocViewerService = TestBed.get(AdsDocViewerService);
    element = fixture.elementRef;
  });

  it('should renderFile() is calling', () => {
    const directive = new AdsDocViewerDirective(renderer, element, adsDocViewerService);

    spyOn(adsDocViewerService, 'getFile').and.returnValue(of(expectedMdFile));
    fixture.detectChanges();

    directive.renderFile(component.mdFile);
    expect(element.nativeElement.innerHTML).toEqual(expectedHtmlFile);
  });
});
