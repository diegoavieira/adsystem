import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AdsDocViewerService } from './ads-doc-viewer.service';

const expectedMdFile = '# Test';
const expectedHtmlFile = '<h1>Test</h1>\n';

describe('AdsDocViewerService', () => {
  let injector: TestBed;
  let service: AdsDocViewerService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AdsDocViewerService]
    });

    injector = getTestBed();
    service = injector.get(AdsDocViewerService);
    httpMock = injector.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should getFile() return md file', () => {
    const path = 'docs/test.md';
    service.getFile(path).subscribe((file) => {
      expect(file).toEqual(expectedMdFile);
    });

    const req = httpMock.expectOne(path);
    expect(req.request.method).toBe('GET');
    req.flush(expectedMdFile);
  });

  it('should renderFile() return html file', () => {
    const renderFile = service.renderFile(expectedMdFile);
    expect(renderFile).toEqual(expectedHtmlFile);
  });
});
