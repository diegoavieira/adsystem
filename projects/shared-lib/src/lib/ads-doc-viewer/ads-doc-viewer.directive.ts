import { Directive, Input, Renderer2, ElementRef, SimpleChanges, OnChanges } from '@angular/core';
import { AdsDocViewerService } from './ads-doc-viewer.service';

@Directive({
  selector: '[adsDocViewer]'
})
export class AdsDocViewerDirective implements OnChanges {
  @Input() adsDocViewer: string;

  constructor(private renderer: Renderer2, private el: ElementRef, private adsDocViewerService: AdsDocViewerService) {}

  ngOnChanges(changes: SimpleChanges) {
    const adsDocViewerCurrentValue = changes.adsDocViewer && changes.adsDocViewer.currentValue;
    this.adsDocViewerService.getFile(adsDocViewerCurrentValue).subscribe((file) => this.renderFile(file));
  }

  renderFile(file: string) {
    const htmlMarked = this.adsDocViewerService.renderFile(file);
    this.renderer.setProperty(this.el.nativeElement, 'innerHTML', htmlMarked);
  }
}
