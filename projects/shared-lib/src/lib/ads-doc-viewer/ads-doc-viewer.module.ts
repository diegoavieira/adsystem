import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdsDocViewerService } from './ads-doc-viewer.service';
import { AdsDocViewerDirective } from './ads-doc-viewer.directive';

@NgModule({
  declarations: [AdsDocViewerDirective],
  imports: [CommonModule],
  providers: [AdsDocViewerService],
  exports: [AdsDocViewerDirective]
})
export class AdsDocViewerModule {}
