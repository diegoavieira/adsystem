import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as marked from 'marked';

const Marked = marked;

@Injectable()
export class AdsDocViewerService {
  constructor(private http: HttpClient) {}

  getFile(path: string): Observable<string> {
    return this.http.get(path, { responseType: 'text' });
  }

  renderFile(file: string): string {
    return Marked(file, { headerIds: false });
  }
}
