# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.0.0](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.33...v1.0.0) (2020-10-28)

### Features

- updated to angular 9 ([bf9bd78](https://gitlab.com/diegoavieira/adsystem/commit/bf9bd7808092bdd16f00661e5c422a393877709c))

## [0.0.33](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.32...v0.0.33) (2020-10-26)

**Note:** Version bump only for package @adsystem/common

## [0.0.32](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.31...v0.0.32) (2020-10-26)

**Note:** Version bump only for package @adsystem/common

## [0.0.31](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.30...v0.0.31) (2020-10-26)

**Note:** Version bump only for package @adsystem/common

## [0.0.30](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.29...v0.0.30) (2020-10-26)

**Note:** Version bump only for package @adsystem/common

## [0.0.29](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.28...v0.0.29) (2020-10-26)

**Note:** Version bump only for package @adsystem/common

## [0.0.28](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.27...v0.0.28) (2020-10-26)

**Note:** Version bump only for package @adsystem/common

## [0.0.27](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.26...v0.0.27) (2020-10-26)

**Note:** Version bump only for package @adsystem/common

## [0.0.26](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.25...v0.0.26) (2020-10-26)

**Note:** Version bump only for package @adsystem/common

## [0.0.25](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.24...v0.0.25) (2020-10-26)

**Note:** Version bump only for package @adsystem/common

## [0.0.24](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.23...v0.0.24) (2020-10-26)

**Note:** Version bump only for package @adsystem/common

## [0.0.23](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.22...v0.0.23) (2020-10-26)

**Note:** Version bump only for package @adsystem/common

## [0.0.22](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.21...v0.0.22) (2020-10-26)

**Note:** Version bump only for package @adsystem/common

## [0.0.21](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.20...v0.0.21) (2020-10-23)

**Note:** Version bump only for package @adsystem/common

## [0.0.20](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.19...v0.0.20) (2020-10-23)

**Note:** Version bump only for package @adsystem/common

## [0.0.19](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.18...v0.0.19) (2020-10-23)

**Note:** Version bump only for package @adsystem/common

## [0.0.18](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.17...v0.0.18) (2020-10-23)

**Note:** Version bump only for package @adsystem/common

## [0.0.17](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.16...v0.0.17) (2020-10-23)

**Note:** Version bump only for package @adsystem/common

## [0.0.16](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.15...v0.0.16) (2020-10-23)

**Note:** Version bump only for package @adsystem/common

## [0.0.15](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.14...v0.0.15) (2020-10-23)

**Note:** Version bump only for package @adsystem/common

## [0.0.14](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.12...v0.0.14) (2020-10-23)

### Bug Fixes

- adjustments ([0a90245](https://gitlab.com/diegoavieira/adsystem/commit/0a90245e3ed6734ad0c2d0ae1eed94fca82a6164))

## [0.0.13](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.12...v0.0.13) (2020-10-23)

**Note:** Version bump only for package @adsystem/common

## [0.0.12](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.11...v0.0.12) (2020-10-23)

**Note:** Version bump only for package @adsystem/common

## [0.0.11](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.10...v0.0.11) (2020-10-23)

**Note:** Version bump only for package @adsystem/common

## [0.0.10](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.9...v0.0.10) (2020-10-23)

**Note:** Version bump only for package @adsystem/common

## [0.0.9](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.8...v0.0.9) (2020-10-22)

### Bug Fixes

- adjustments ([afc66fc](https://gitlab.com/diegoavieira/adsystem/commit/afc66fc5ac515bf7d869b6010c3984acb8304f68))

## [0.0.8](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.7...v0.0.8) (2020-07-07)

**Note:** Version bump only for package @adsystem/common

## [0.0.7](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.6...v0.0.7) (2020-06-04)

**Note:** Version bump only for package @adsystem/common

## [0.0.6](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.5...v0.0.6) (2020-04-30)

### Bug Fixes

- created doc-viewer ([420e3fa](https://gitlab.com/diegoavieira/adsystem/commit/420e3fa4c03335e39b5033cd594561af037e4848))

## [0.0.5](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.4...v0.0.5) (2020-04-21)

### Bug Fixes

- adjustments ([d612d40](https://gitlab.com/diegoavieira/adsystem/commit/d612d40a83319b8166a696539f78c6df36fac83b))

## [0.0.4](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.3...v0.0.4) (2020-04-20)

### Bug Fixes

- adjustments ([8676a35](https://gitlab.com/diegoavieira/adsystem/commit/8676a352961a2e3375cc770b1296dc649cd9856a))

## [0.0.3](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.2...v0.0.3) (2020-04-19)

### Bug Fixes

- created theming directive ([099d21b](https://gitlab.com/diegoavieira/adsystem/commit/099d21b48e4cd2f158d0cb26844c190691282fc3))

## [0.0.2](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.1...v0.0.2) (2020-04-17)

**Note:** Version bump only for package @adsystem/common

## 0.0.1 (2020-04-17)

**Note:** Version bump only for package @adsystem/common
