export interface AdsTheming {
  primary: string;
  accent: string;
  warn?: string;
  text?: string;
  background?: string;
}
