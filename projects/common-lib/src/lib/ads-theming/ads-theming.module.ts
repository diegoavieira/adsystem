import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdsThemingDirective } from './ads-theming.directive';

@NgModule({
  declarations: [AdsThemingDirective],
  imports: [CommonModule],
  exports: [AdsThemingDirective]
})
export class AdsThemingModule {}
