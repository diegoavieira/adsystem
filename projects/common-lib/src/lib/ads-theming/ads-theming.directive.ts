import { Directive, OnChanges, Input, Renderer2, ElementRef, SimpleChanges } from '@angular/core';
import * as tinycolor2 from 'tinycolor2';
import { AdsTheming } from './ads-theming';

const tinycolor = tinycolor2;

interface ColorPalette {
  name: string;
  hex: string;
  darkContrast: boolean;
}

@Directive({
  selector: '[adsTheming]'
})
export class AdsThemingDirective implements OnChanges {
  @Input() adsTheming: AdsTheming;
  @Input() adsDarkTheming: boolean;

  constructor(private renderer: Renderer2, private el: ElementRef) {}

  ngOnChanges(changes: SimpleChanges) {
    this.setTheme(changes.adsTheming && changes.adsTheming.currentValue);
    this.setDarkTheme(changes.adsDarkTheming && changes.adsDarkTheming.currentValue);
  }

  setTheme(theme: AdsTheming) {
    const stylesProperty: Array<string> = [];

    for (const key of Object.keys(theme)) {
      if (key === 'text' || key === 'background') {
        stylesProperty.push(`--ads-${key}: ${theme[key]}`);
      } else {
        stylesProperty.push(...this.setColors(theme, key));
      }
    }

    this.renderer.setProperty(this.el.nativeElement, 'style', stylesProperty.join(';'));
  }

  setDarkTheme(isDark: boolean) {
    if (isDark) {
      this.renderer.addClass(this.el.nativeElement, 'ads-dark-theme');
    } else {
      this.renderer.removeClass(this.el.nativeElement, 'ads-dark-theme');
    }
  }

  setColors(theme: AdsTheming, key: string): Array<string> {
    const stylesProperty: Array<string> = [];
    const colorsPalette: ColorPalette[] = this.createColors(theme[key]);

    for (const color of colorsPalette) {
      const colorHex = color.hex;
      const colorDarkContrast = color.darkContrast ? 'rgba(black, 0.87)' : 'white';

      stylesProperty.push(`--ads-${key}-${color.name}: ${colorHex}`);
      stylesProperty.push(`--ads-${key}-contrast-${color.name}: ${colorDarkContrast}`);
    }

    return stylesProperty;
  }

  createColors(hex: string): ColorPalette[] {
    return [
      this.getColors(tinycolor(hex).lighten(52), '50'),
      this.getColors(tinycolor(hex).lighten(37), '100'),
      this.getColors(tinycolor(hex).lighten(26), '200'),
      this.getColors(tinycolor(hex).lighten(12), '300'),
      this.getColors(tinycolor(hex).lighten(6), '400'),
      this.getColors(tinycolor(hex), '500'),
      this.getColors(tinycolor(hex).darken(6), '600'),
      this.getColors(tinycolor(hex).darken(12), '700'),
      this.getColors(tinycolor(hex).darken(18), '800'),
      this.getColors(tinycolor(hex).darken(24), '900'),
      this.getColors(tinycolor(hex).lighten(50).saturate(30), 'A100'),
      this.getColors(tinycolor(hex).lighten(30).saturate(30), 'A200'),
      this.getColors(tinycolor(hex).lighten(10).saturate(15), 'A400'),
      this.getColors(tinycolor(hex).lighten(5).saturate(5), 'A700')
    ];
  }

  getColors(value: any, name: string): ColorPalette {
    const color = tinycolor(value);

    return {
      name,
      hex: color.toHexString(),
      darkContrast: color.isLight()
    };
  }
}
