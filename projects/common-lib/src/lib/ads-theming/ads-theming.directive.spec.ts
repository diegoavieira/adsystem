import { AdsThemingDirective } from './ads-theming.directive';
import { Component, ElementRef, Renderer2, Type } from '@angular/core';
import { AdsTheming } from './ads-theming';
import { ComponentFixture, TestBed } from '@angular/core/testing';

@Component({})
class TestComponent {
  theme: AdsTheming = {
    primary: 'red',
    accent: 'blue',
    warn: 'red',
    text: 'black',
    background: 'gray'
  };
  darkTheme = false;
}

describe('AdsThemingDirective', () => {
  let component: TestComponent;
  let element: ElementRef;
  let renderer: Renderer2;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(() => {
    TestBed.overrideComponent(TestComponent, {
      set: {
        template: `<div [adsTheming]="theme" [adsDarkTheming]="darkTheme"></div>`
      }
    });

    TestBed.configureTestingModule({
      declarations: [TestComponent, AdsThemingDirective]
    });

    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
    renderer = fixture.componentRef.injector.get<Renderer2>(
      Renderer2 as Type<Renderer2>
    );
    element = fixture.elementRef;

    fixture.detectChanges();
  });

  it('should setTheme() is calling', () => {
    const directive = new AdsThemingDirective(renderer, element);

    directive.setTheme(component.theme);
    expect(element.nativeElement.style).toBeTruthy();
  });

  it('should setDarkTheme() is calling', () => {
    const directive = new AdsThemingDirective(renderer, element);

    component.darkTheme = true;

    directive.setDarkTheme(component.darkTheme);
    expect(element.nativeElement.className).toEqual('ads-dark-theme');
  });
});
