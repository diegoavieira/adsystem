import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdsToolbarComponent } from './ads-toolbar.component';
import { MatToolbarModule } from '@angular/material/toolbar';

@NgModule({
  declarations: [AdsToolbarComponent],
  imports: [CommonModule, MatToolbarModule],
  exports: [AdsToolbarComponent]
})
export class AdsToolbarModule {}
