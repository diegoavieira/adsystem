import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdsToolbarComponent } from './ads-toolbar.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('AdsToolbarComponent', () => {
  let component: AdsToolbarComponent;
  let fixture: ComponentFixture<AdsToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdsToolbarComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdsToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
