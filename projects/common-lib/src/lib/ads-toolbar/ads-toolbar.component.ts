import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ads-toolbar',
  templateUrl: './ads-toolbar.component.html',
  styleUrls: ['./ads-toolbar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AdsToolbarComponent implements OnInit {
  @Input() title: string;

  constructor() {}

  ngOnInit() {}
}
