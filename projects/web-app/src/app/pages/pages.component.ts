import { Component, OnInit } from '@angular/core';
import { AdsTheming } from '@adsystem/common';

@Component({
  selector: 'web-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {
  mainTheme: AdsTheming;
  isDarkTheme: boolean;

  constructor() {}

  ngOnInit() {
    this.mainTheme = {
      primary: 'blue',
      accent: 'green'
    };

    this.isDarkTheme = false;
  }
}
