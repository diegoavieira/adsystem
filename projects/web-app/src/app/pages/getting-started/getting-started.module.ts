import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GettingStartedRoutingModule } from './getting-started-routing.module';
import { GettingStartedComponent } from './getting-started.component';
import { AdsDocViewerModule } from '@adsystem/shared';

@NgModule({
  declarations: [GettingStartedComponent],
  imports: [CommonModule, GettingStartedRoutingModule, AdsDocViewerModule],
  providers: []
})
export class GettingStartedModule {}
