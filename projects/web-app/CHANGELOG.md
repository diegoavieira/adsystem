# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.0.0](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.33...v1.0.0) (2020-10-28)

### Features

- updated to angular 9 ([bf9bd78](https://gitlab.com/diegoavieira/adsystem/commit/bf9bd7808092bdd16f00661e5c422a393877709c))

## [0.0.33](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.32...v0.0.33) (2020-10-26)

**Note:** Version bump only for package @adsystem/web

## [0.0.32](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.31...v0.0.32) (2020-10-26)

**Note:** Version bump only for package @adsystem/web

## [0.0.31](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.30...v0.0.31) (2020-10-26)

**Note:** Version bump only for package @adsystem/web

## [0.0.30](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.29...v0.0.30) (2020-10-26)

**Note:** Version bump only for package @adsystem/web

## [0.0.29](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.28...v0.0.29) (2020-10-26)

**Note:** Version bump only for package @adsystem/web

## [0.0.28](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.27...v0.0.28) (2020-10-26)

**Note:** Version bump only for package @adsystem/web

## [0.0.27](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.26...v0.0.27) (2020-10-26)

**Note:** Version bump only for package @adsystem/web

## [0.0.26](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.25...v0.0.26) (2020-10-26)

**Note:** Version bump only for package @adsystem/web

## [0.0.25](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.24...v0.0.25) (2020-10-26)

**Note:** Version bump only for package @adsystem/web

## [0.0.24](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.23...v0.0.24) (2020-10-26)

**Note:** Version bump only for package @adsystem/web

## [0.0.23](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.22...v0.0.23) (2020-10-26)

**Note:** Version bump only for package @adsystem/web

## [0.0.22](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.21...v0.0.22) (2020-10-26)

**Note:** Version bump only for package @adsystem/web

## [0.0.21](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.20...v0.0.21) (2020-10-23)

**Note:** Version bump only for package @adsystem/web

## [0.0.20](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.19...v0.0.20) (2020-10-23)

**Note:** Version bump only for package @adsystem/web

## [0.0.19](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.18...v0.0.19) (2020-10-23)

**Note:** Version bump only for package @adsystem/web

## [0.0.18](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.17...v0.0.18) (2020-10-23)

**Note:** Version bump only for package @adsystem/web

## [0.0.17](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.16...v0.0.17) (2020-10-23)

**Note:** Version bump only for package @adsystem/web

## [0.0.16](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.15...v0.0.16) (2020-10-23)

**Note:** Version bump only for package @adsystem/web

## [0.0.15](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.14...v0.0.15) (2020-10-23)

**Note:** Version bump only for package @adsystem/web

## [0.0.14](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.12...v0.0.14) (2020-10-23)

### Bug Fixes

- adjustments ([0a90245](https://gitlab.com/diegoavieira/adsystem/commit/0a90245e3ed6734ad0c2d0ae1eed94fca82a6164))

## [0.0.13](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.12...v0.0.13) (2020-10-23)

**Note:** Version bump only for package @adsystem/web

## [0.0.12](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.11...v0.0.12) (2020-10-23)

**Note:** Version bump only for package @adsystem/web

## [0.0.11](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.10...v0.0.11) (2020-10-23)

**Note:** Version bump only for package @adsystem/web

## [0.0.10](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.9...v0.0.10) (2020-10-23)

**Note:** Version bump only for package @adsystem/web

## [0.0.9](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.8...v0.0.9) (2020-10-22)

**Note:** Version bump only for package @adsystem/web

## [0.0.8](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.7...v0.0.8) (2020-07-07)

**Note:** Version bump only for package @adsystem/web

## [0.0.7](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.6...v0.0.7) (2020-06-04)

### Bug Fixes

- adjustmens ([b2f91f6](https://gitlab.com/diegoavieira/adsystem/commit/b2f91f61af118fe7c4479ec57cde865bad03779d))
- adjustments ([815c032](https://gitlab.com/diegoavieira/adsystem/commit/815c032e269ac9741c9325b31dceb8336c651877))
- adjustments ([c812deb](https://gitlab.com/diegoavieira/adsystem/commit/c812deb940ab3b19761144fb7520f4942e128830))
- adjustments ([5194cfd](https://gitlab.com/diegoavieira/adsystem/commit/5194cfdaeafee6bc8a22d9574bcb871fcf8542e6))
- adjustments ([d100a62](https://gitlab.com/diegoavieira/adsystem/commit/d100a62e9bec77e92f6e046356d4459a5dc673d0))
- adjustments ([fb9e6f5](https://gitlab.com/diegoavieira/adsystem/commit/fb9e6f5297c8be487a4ba3ef49097c6d3395ac15))
- adjustments ([30c2bd5](https://gitlab.com/diegoavieira/adsystem/commit/30c2bd548508c96dec1c601a3d4beeb1c9dc3376))
- adjustments ([b5a543f](https://gitlab.com/diegoavieira/adsystem/commit/b5a543f8d82bec5bc826615db3eaf084cd0e34bf))
- adjustments ([d71719d](https://gitlab.com/diegoavieira/adsystem/commit/d71719d20847262f250cc97d9461fbca159e1a27))
- adjustments ([0c2164a](https://gitlab.com/diegoavieira/adsystem/commit/0c2164ab12703aa26c1fbe28984c5a3d5e7b9e18))
- adjustments ([15f7351](https://gitlab.com/diegoavieira/adsystem/commit/15f735106dba34ca2d6e2a67eb0f90549c097b62))
- adjustments ([7f4e229](https://gitlab.com/diegoavieira/adsystem/commit/7f4e2291640ee1ed155f2c22bf3c8da6202d8af3))
- adjustments ([cbffd7a](https://gitlab.com/diegoavieira/adsystem/commit/cbffd7a00192ebb3be0c9c8c68929d1dabe9a888))
- adjustments ([46598c1](https://gitlab.com/diegoavieira/adsystem/commit/46598c1b5a57a8be579982095758cf3d64dc5c6b))
- adjustments ([d300685](https://gitlab.com/diegoavieira/adsystem/commit/d300685fb78b88198f4faecadc5a6acfe56499e0))
- adjustments ([e19a50e](https://gitlab.com/diegoavieira/adsystem/commit/e19a50e7e4435e0224cf5e51b3ad399466c180b7))

## [0.0.6](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.5...v0.0.6) (2020-04-30)

### Bug Fixes

- created doc-viewer ([420e3fa](https://gitlab.com/diegoavieira/adsystem/commit/420e3fa4c03335e39b5033cd594561af037e4848))

## [0.0.5](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.4...v0.0.5) (2020-04-21)

### Bug Fixes

- adjustments ([d612d40](https://gitlab.com/diegoavieira/adsystem/commit/d612d40a83319b8166a696539f78c6df36fac83b))

## [0.0.4](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.3...v0.0.4) (2020-04-20)

**Note:** Version bump only for package @adsystem/web

## [0.0.3](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.2...v0.0.3) (2020-04-19)

### Bug Fixes

- created theming directive ([099d21b](https://gitlab.com/diegoavieira/adsystem/commit/099d21b48e4cd2f158d0cb26844c190691282fc3))

## [0.0.2](https://gitlab.com/diegoavieira/adsystem/compare/v0.0.1...v0.0.2) (2020-04-17)

**Note:** Version bump only for package @adsystem/web

## 0.0.1 (2020-04-17)

**Note:** Version bump only for package @adsystem/web
